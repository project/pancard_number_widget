<?php

namespace Drupal\pancard_number_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'pancard_number_widget' widget.
 *
 * @FieldWidget(
 *   id = "pancard_number_widget",
 *   module = "pancard_number_widget",
 *   label = @Translation("Pancard Number Widget"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class PancardNumberWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['value'] = $element + [
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->value ?? NULL,
      '#maxlength' => 10,
      '#element_validate' => [
        [static::class, 'validate'],
      ],
    ];

    return $element;
  }

  /**
   * Validate the color text field.
   */
  public static function validate($element, FormStateInterface $form_state) {
    $value = $element['#value'];

    // Return if value is empty.
    if (strlen($value) == 0) {
      return;
    }

    $pattern = "/[A-Z]{5}[0-9]{4}[A-Z]{1}/";

    if (preg_match($pattern, $value)) {
      return;
    }
    else {
      $form_state->setError($element, t("The Pancard number is not valid. you can follow the bellow pattern. <br>Pancard Pattern:- BNZAA2318J"));
    }

  }

}
