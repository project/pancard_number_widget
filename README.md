# Pancard Number Widget

A PAN card (short for Permanent Account Number card) is a personal
identification document issued by the IndianIncome Tax Department to
individuals and organizations in India. It is a 10-digit alphanumeric
identificationnumber that is unique to each individual or organization
and is used for various purposes, such as filing incometax returns and
conducting financial transactions.

The PAN card is a laminated plastic card that contains the individual's
or organization's name, date of birth (forindividuals), and PAN number.
It also contains a photograph of the individual (for individuals) and
otheridentifying information. The PAN card is issued to individuals and
organizations upon request and is valid for thelifetime of the holder.

In India, the PAN card is an important document that is widely used for
various purposes, such as opening a bankaccount, applying for a credit
card, purchasing a vehicle or property, and conducting other financialtransactions.
It is also used as a valid proof of identity and address for various purposes,
such as obtaining apassport, driver's license, and other government-issued documents.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/pancard_number_widget).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/pancard_number_widget).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Allow the text field widget pancard number validate formatter.


## Maintainers

- Omprakash Mankar - [omrmankar](https://www.drupal.org/u/omrmankar)